# Pleroma/Akkoma role
Pleroma/Akkoma role was initially based on role by (Luke Hoersten)[https://src.nth.io/ansible-roles/file/2d705e63f6cb/pleroma/otp] with heavy modifications (basically re-written).

This role deploys and updates OTP akkoma (default) or pleroma instance. Currently supported distributions are debian based. Role requires Disroot's (nginx)[https://git.disroot.org/Disroot-Ansible/nginx] role and (postgresql)[https://github.com/ANXS/postgresql.git]. Other nginx / postgres roles could be used but may require changes in variables.

You can deploy test instance using `Vagrantfile` attached to the role.
`vagrant up`

`ansible-playbook -b Playbooks/pleroma.yml`

Then you can then access gitea from your computer on `http://192.168.33.15`

To update pleroma include `upgrade` **tag**.


## Frontend
This role also deploys Mangane as the default frontend (this can be changed with `pleroma_frontends` var). Mangane can be configured in `defaults/main.yml` (See the `# Mangane vars` section).

⚠️ If you have already deployed Mangane and have changed config from http://192.168.33.15/soapbox/config then you have to first remove this config from DB with `sudo -u pleroma /opt/pleroma/release/bin/pleroma_ctl config delete pleroma frontend_configurations`. Then run the role with `ansible-playbook -b Playbooks/pleroma.yml --tags upgrade`


## Customization
In the `files` folder, `avi.png` is the default user avatar. You can also changes logo that you can find there.

If you have already deployed but want to do any change, like change logo, avatar or a config, then run the role with `ansible-playbook -b Playbooks/pleroma.yml --tags upgrade`.